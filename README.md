# Dialecta Database

This is a Django application for oral corpora in ELAN format, initially developed for the TriMCo project (http://www.trimco.uni-mainz.de).

## Installation

### Running the platform on your local computer.

1. Clone this repository

	>$ git clone https://maryszmary@bitbucket.org/maryszmary/trimco.git

	>$ cd trimco

2. Create and activate virtual environment to install the requirements needed:

	>$ virtualenv -p python3 ve

	>$ . ve/bin/activate

3. Install all requirements:

	>$ pip3 install -r requirements.txt

4. Create superuser:

	>$ python3 manage.py createsuperuser

5. Run the server:

	>$ python3 manage.py runserver

6. Go (in your browser) to the address your_server_ip_or_name:8000/admin


### Running the platform on a ubuntu apache2 server.

First, perform the actons 1-4 from the previous section on the server.
There are two ways of running the program on the server, one of them might work for you :)

#### First option

Run the following command:

>$ sudo python3 manage.py runserver your_server_ip_or_name:port

Replace **your_server_ip_or_name** with the IP of your server and **port** with the port you are going to run it on. 
By default, the port is 8000. This port might be already in use, so, you will want to change the port to some other number, e.g. 8090 or 8080.

#### Second option

Follow the instruction [here](https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-ubuntu-14-04#configure-apache). It will require to change the changing the .conf file, which also supposes that you have admin rights.
